@extends('layouts.app')
@section('title', Auth::user()->name .'编辑资料')

@section('content')

<div class="container">
	<div class="panel panel-default col-md-10 col-md-offset-1">
		<div class="panel-heading">
			<h4>
				<i class="glyphicon glyphicon-edit"></i>{{ Auth::user()->name }} · 编辑个人资料
			</h4>

		</div>
		
		@include('common.error')

		<div class="panel-body">
			<form action="{{ route('users.update', $user->id) }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
				<input type="hidden" name="_method" value="PUT">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
					<label for="name-field">用户名</label>
					<input id="name-field" class="form-control" type="text" name="name" value="{{ old('name', $user->name) }}">
				</div>

				<div class="form-group">
					<label for="email-field">电子邮箱</label>
					<input id="email-field" class="form-control" type="email" name="email" value="{{ old('email', $user->email) }}">
				</div>

				<div class="form-group">
					<label for="introduction-field">个人简介</label>
					<textarea id="introduction-field" class="form-control" name="introduction" rows="3">
						{{ old('introduction', $user->introduction) }}
					</textarea>
				</div>

				<div class="form-group">
					<label for="" class="avatar-label">用户头像</label>
					<input type="file" name="avatar">

					@if ($user->avatar)
						<hr>
						<img class="thumbnail img-responsive" src="{{ $user->avatar }}" width="200">
					@endif

				</div>

				<div class="well well-sm">
					<button class="btn btn-primary" type="submit">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>

@stop