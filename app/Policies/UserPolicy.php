<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //当前用户id全等于提取数据id
    public function update(User $currentUser, User $user)
    {
        return $currentUser->id === $user->id;
    }
}
