<?php

namespace App\Models;

class Reply extends Model
{
    protected $fillable = ['content'];

    //reply belongs TO topic
    public function topic()
    {
    	return $this->belongsTo(Topic::class);
    }

    //reply nelongs TO user
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
